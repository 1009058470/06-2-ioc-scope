package com.twuc.webApp.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(scopeName = "prototype",proxyMode = ScopedProxyMode.TARGET_CLASS )
public class PrototypeDependentWithProxy{

    private MyLogger myLogger;


    public PrototypeDependentWithProxy(MyLogger myLogger) {
        this.myLogger = myLogger;
        this.myLogger.getLogger().add("this is a new bean");
    }

    void someMethod(){

    }
}

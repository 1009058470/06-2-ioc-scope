package com.twuc.webApp.entity;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxy {

    private PrototypeDependentWithProxy prototypeDependentWithProxy;

    public SingletonDependsOnPrototypeProxy(PrototypeDependentWithProxy prototypeDependentWithProxy) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
    }

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }

    public void someMethod(){
        prototypeDependentWithProxy.someMethod();
    }


}

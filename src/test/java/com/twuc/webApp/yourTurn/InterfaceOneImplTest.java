package com.twuc.webApp.yourTurn;

import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class InterfaceOneImplTest {

    @Test
    void test_if_equals_one_imp_and_one() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
        InterfaceOne bean1 = context.getBean(InterfaceOneImp.class);
        InterfaceOne bean2 = context.getBean(InterfaceOne.class);
        assertEquals(bean1,bean2);
    }
}
